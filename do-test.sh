:
#
CCACHE_DIR=/tmp/test-cache-dir

CCACHE_LOGFILE=${CCACHE_DIR}/logfile
export CCACHE_LOGFILE

CCACHE_COMPRESS=true
export CCACHE_COMPRESS


ccache -C

# make clean
rm -f *.dwo
rm -f *.o

# should re-create all the objects
# CCACHE=/usr/bin/ccache
CCACHE=$HOME/repos/ccache-gsplit-dwarf-support/ccache
SPLITDWARF=-gsplit-dwarf
# SPLITDWARF=

set -vx

time make CC="$CCACHE /usr/bin/gcc-4.8 $SPLITDWARF" 

# should use the cache for all the objects

rm -f *.o *.dwo
# make clean


time make CC="$CCACHE /usr/bin/gcc-4.8 $SPLITDWARF" 


