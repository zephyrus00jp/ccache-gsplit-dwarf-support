:
#
:
#
CCACHE_DIR=/tmp/test-cache-dir

CCACHE_LOGFILE=${CCACHE_DIR}/logfile
export CCACHE_LOGFILE

CCACHE_COMPRESS=true
export CCACHE_COMPRESS


if [ -s ./ccache ]
then
    echo ccache exists
else
    echo ccache does not exist. So creating it ...
    make ccache
fi


./ccache -C

# make clean
rm -f *.dwo
rm -f *.o

# should re-create all the objects
# CCACHE=/usr/bin/ccache
CCACHE=./ccache
SPLITDWARF=-gsplit-dwarf
# SPLITDWARF=

set -vx

time make CC="$CCACHE /usr/bin/gcc-4.8 $SPLITDWARF"

# should use the cache for all the objects

rm -f *.o *.dwo
# make clean


time make CC="$CCACHE /usr/bin/gcc-4.8 $SPLITDWARF"

CC="$CCACHE /usr/bin/gcc-4.8 $SPLITDWARF"
#
#
# Testing in March, 2015
#
# clean cache
# ccache cc test-file-1.c  => test-file-1.o test-file-1.dwo in cache
# Result: cache miss
$CC -c test-file-1.c
# ccache cc test-file-2.c  => test-file-2.o test-file-2.dwo in cache
# Result: cache miss
$CC -c test-file-2.c

#
#ccache cc test-file-1.c  => Reuse the files in cache to obtain
#       	  		    test-file-1.o and test-file-1.dwo
# Result: cache hit (direct)
rm -f test-file-1.s test-file-1.dwo test-file-1.o
$CC -c test-file-1.c
if [ -s test-file-1.o ]
then
    echo OK
else
    echo failure to obtain .o
    exit 1
fi
if [ -s test-file-1.dwo ]
then
    echo OK
else
    echo failure to obtain .dwo
    exit 1
fi
#
#
# modify test-file-2.c
# add unique global variable
VAR=var_$(env LANG=C date +%m%d%Y%H%M%s)
echo "int $VAR; " >> test-file-2.c
sleep 2
# ccache cc test-file-2.c  => test-file-2.o (NEW) test-file-2.dow (NEW)
#       			    in cache.
# Result: cache miss
$CC -c test-file-2.c

# modify test-file-1.c
VAR=var_$(env LANG=C date +%m%d%Y%H%M%s)
echo "int $VAR; " >> test-file-1.c
sleep 2
#
#ccache cc test-file-1.c  => test-file-1.o (NEW) test-file-1.dow (NEW)
#       			    in cache.
# Result: cache miss
$CC -c test-file-1.c
#
#ccache cc test-file-2.c  => Reuse the files in cache to obtain
#       	  		    test-file-2.o and test-file-2.dwo
# Result: cache hit (direct)
rm -f test-file-2.o test-file-2.dwo
$CC -c test-file-2.c
if [ -s test-file-2.o ]
then
    echo OK
else
    echo failure to obtain .o
    exit 1
fi
if [ -s test-file-2.dwo ]
then
    echo OK
else
    echo failure to obtain .dwo
    exit 1
fi
#
# ccache cc -S test-file-2.c  => test-file-2.s
# Result: cache miss
#
$CC -c -S test-file-2.c
#
#
rm -f test-file-2.s test-file-2.o
# ccache cc -S test-file-2.c  => Reuse test-file-2.s
# Result: cache hit (direct)
rm -f test-file-2.s test-file-2.dwo
$CC -c -S test-file-2.c
if [ -s test-file-2.s ]
then
    echo OK
else
    echo "Failure to obtain .s"
    exit 1
fi
# we don't get .dwo file when "-S" is given to compiler
#
# modify test-file-2.c
VAR=var_$(env LANG=C date +%m%d%Y%H%M%s)
echo "int $VAR; " >> test-file-2.c
sleep 2
#
# ccache cc -c -S test-file-2.c  => test-file-2.s (new)
# Result: cache miss
$CC -c -S test-file-2.c
#
echo
echo "logfile"
echo
egrep "Result:" $CCACHE_LOGFILE | tail -30
echo
egrep -i Result: ./do-test-split-dwarf.sh
